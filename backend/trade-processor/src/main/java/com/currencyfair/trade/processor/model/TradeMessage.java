package com.currencyfair.trade.processor.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradeMessage {
	@NotNull
	private String userId;
	@NotNull
	private String currencyFrom;
	@NotNull
	private String currencyTo;
	@NotNull
	private double amountSell;
	@NotNull
	private double amountBuy;
	@NotNull
	private double rate;
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern="dd-MMM-yy HH:mm:ss")
	private Date timePlaced;
	@NotNull
	private String originatingCountry;
}
