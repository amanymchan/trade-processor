package com.currencyfair.trade.processor.service;

import com.currencyfair.trade.processor.model.TradeMessage;

public interface MessageService {
	
	void addMessage(TradeMessage message);

}
