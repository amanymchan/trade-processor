package com.currencyfair.trade.processor.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.currencyfair.trade.processor.model.TradeMessage;
import com.currencyfair.trade.processor.service.MessageService;

@Path("/messages")
public class MessageResource {
	
	@Autowired MessageService messageSvc;
	
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEmployee(TradeMessage message) {
		messageSvc.addMessage(message);
        return Response.status(Response.Status.CREATED.getStatusCode()).build();
    }

}
