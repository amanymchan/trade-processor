package com.currencyfair.trade.processor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.currencyfair.trade.processor.model.TradeMessage;

@Component
public class MessageServiceImpl implements MessageService {
	
	@Autowired SimpMessagingTemplate simpMessagingTemplate;

	@Override
	public void addMessage(TradeMessage message) {
	    simpMessagingTemplate.convertAndSendToUser(message.getUserId(), "/queue/message", message);
	}
}
