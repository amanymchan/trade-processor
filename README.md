# README #


### What is this repository for? ###

The repository contains source project codes for both frontend and backend implementation as a trade message processor and visualizer

* <b><u>Backend (Spring Boot, Jersey, WebSocket)</u></b> <br/>
The backend is built with Spring Boot as standalone microservice, RESTful APIs are built upon Jersey which is standard JAXRS implementation where a POST API endpoint "/messages" is exposed for posting trade message externally. Upon message received, message would be sent to frontend client using STOMP(websocket) implementation as part of backend implementation. WebSocket connection would be initiated from frontend and secured by token(for simplicity, only one user "amanchan" is hardcoded throughout the app) in http header suring websocket handshake, and frontend would subscribe the message as sent subsequently. For simplicity, authentication/authorization(which could be implemented with Spring Security / JAXRS Providers) is omitted and database storage(which could be done with Spring Data, Repository) is omitted.

* <b><u>Frontend (ReactJS, STOMP Client, React Google Charts)</u></b> <br/>
The frontend is built with ReactJS, and components are created for list of message consumed, websocket connection and statistic charts, services are created to maintain user messages/app state(could be refactored with Redux in general) for simplicity and for processing of message from WebSocket backend. For simplicity, no user login is implemented and assume "amanchan" is already logged in and user "amanchan" is used for WebSocket handshake.

* <b><u>Unit Testing</u></b> <br/>
Unit test cases are yet to added to both projects, but in genereal, backend component(class) could be tested independently with scenarios using JUnit/Mockito for uniting testing public methods with mocked dependencies(e.g services/static methods), and each frontend component/service could be unit tested or e2e tesed with jest test runner independently also with mocked depencicies and verified with dom elements. For both frontend and backend unit testing, setups on e.g mocked dependencies could be setup before any test and cleanup could be done after all test.

### How do I get set up? ###

* Start backend service <br/>
to start SpringBoot service, clone the repository and start with command
```
> cd backend/trade-processor 
> ./gradlew bootRun
```
<br/>
*  Use CURL to post message to backend:
```
curl --location --request POST 'http://localhost:8080/api/messages' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userId": "amanchan",
    "currencyFrom": "EUR",
    "currencyTo": "HKD",
    "amountSell": 1000,
    "amountBuy": 747.10,
    "rate": 0.7471,
    "timePlaced": "24-JAN-18 10:27:44",
    "originatingCountry": "FR"
}'
```
<br/>
* Start frontend <br/>
to start ReactJS app, clone the repository and start with command
```
> cd frontend/trade-processor
> npm install
> npm start
```
<br/>
By posting message to API, frontend should automatically reflected the list of messages and summary chart of currencies sold and bought.
![Screenshot](screenshot.png)
