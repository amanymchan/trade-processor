"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
var rxjs_1 = require("rxjs");
//for simplication, maintain app state here instead of redux
var MessageService = /** @class */ (function () {
    function MessageService() {
        var _this = this;
        this.state = {
            messages: [],
            stat: {
                sell: {},
                buy: {}
            }
        };
        this._stateChanged = new rxjs_1.BehaviorSubject(this.state);
        this.addMessage = function (message) {
            var messages = __spreadArrays(_this.state.messages, [
                message
            ]);
            var sell = messages.reduce(function (prev, cur) {
                if (prev[cur.currencyFrom] === undefined) {
                    prev[cur.currencyFrom] = 0;
                }
                prev[cur.currencyFrom] += cur.amountSell;
                return prev;
            }, {});
            var buy = messages.reduce(function (prev, cur) {
                if (prev[cur.currencyTo] === undefined) {
                    prev[cur.currencyTo] = 0;
                }
                prev[cur.currencyTo] += cur.amountBuy;
                return prev;
            }, {});
            var stat = { sell: sell, buy: buy };
            _this.state = {
                messages: messages,
                stat: stat
            };
            _this.stateChanged.next(_this.state);
        };
    }
    MessageService.instance = function () {
        if (!MessageService._instance) {
            MessageService._instance = new MessageService();
        }
        return MessageService._instance;
    };
    Object.defineProperty(MessageService.prototype, "stateChanged", {
        get: function () { return this._stateChanged; },
        enumerable: false,
        configurable: true
    });
    MessageService.prototype.MessageService = function () { };
    return MessageService;
}());
exports["default"] = MessageService;
