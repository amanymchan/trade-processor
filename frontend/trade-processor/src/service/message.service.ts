import { BehaviorSubject } from 'rxjs';

    //for simplication, maintain app state here instead of redux
export default class MessageService{
    
    private static _instance:MessageService;

    public static instance(): MessageService {
        if(!MessageService._instance) {
            MessageService._instance = new MessageService();
        }
        return MessageService._instance;
    }

    private state: {messages:any[],stat:{buy:{[key: string]: number},sell:{[key: string]: number}}} = {
        messages: [],
        stat:{
            sell:{},
            buy:{}
        }
    }
    
    private _stateChanged=new BehaviorSubject(this.state);
    public get stateChanged() { return this._stateChanged; }

    private MessageService(){}

    public addMessage = (message:any) => {
        const messages = [
            ...this.state.messages,
            message
        ];
        const sell = messages.reduce((prev,cur)=>{
            if(prev[cur.currencyFrom]===undefined) {
                prev[cur.currencyFrom] = 0;
            }
            prev[cur.currencyFrom] += cur.amountSell;
            return prev;
        },{})
        const buy = messages.reduce((prev,cur)=>{
            if(prev[cur.currencyTo]===undefined) {
                prev[cur.currencyTo] = 0;
            }
            prev[cur.currencyTo] += cur.amountBuy;
            return prev;
        },{});
        const stat = {sell,buy}
        this.state = {
            messages,
            stat
        }
        this.stateChanged.next(this.state);
    }
}