export default class UserService{

    private static _instance:UserService;

    public static instance(): UserService {
        if(!UserService._instance) {
            UserService._instance = new UserService();
        }
        return UserService._instance;
    }
    
    private username:string;

    private UserService(){}

    public authenticatUser = (username:string) => {
        this.username = username;
    }

    public getUserName = () => {
        return this.username;
    }
}