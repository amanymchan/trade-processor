import React , { Component} from 'react';
import { Chart } from "react-google-charts";
import MessageService from '../service/message.service.ts';
import { map, takeUntil, Subject } from 'rxjs';

export default class StatChartComponent extends Component {
  destroy=new Subject();

    constructor(props) {
        super(props);
        this.state = {
            sell:[],
            buy:[]
        }
        MessageService.instance().stateChanged.pipe(
            takeUntil(this.destroy),
            map(state=>state.stat)
        ).subscribe(stat=>{
          const {sell,buy} = stat;
          let sellData = [
            ['Currency','Amount']
          ];
          let buyData = [
            ['Currency','Amount']
          ];
          for (let key in sell) {
              let value = sell[key];
              sellData.push([key,value])
          }
          for (let key in buy) {
              let value = buy[key];
              buyData.push([key,value])
          }
          this.setState({
              sell: sellData,
              buy: buyData
          });
          console.log('stat',this.state)
        });
    }

    componentWillUnmount(){
        this.destroy.next();
        this.destroy.complete();
    }

    render() {
        const showChart = this.state.sell.length>0 && this.state.buy.length>0;
        const content = !showChart? 
                      <div></div> :
                      <div style={{display:'flex', flexDirection:'column'}}>
                        <div style={{flex:'1'}}>
                          <Chart
                            width={'400px'}
                            height={'300px'}
                            chartType="PieChart"
                            loader={<div>Loading Chart</div>}
                            data = {this.state.sell}
                            options={{
                              backgroundColor: 'transparent',
                              title: 'Currency Sold',
                              sliceVisibilityThreshold: 0
                            }}
                            rootProps={{ 'data-testid': '7' }}
                          />
                        </div>
                        <div style={{flex:'1'}}>
                          <Chart
                            width={'400px'}
                            height={'300px'}
                            chartType="PieChart"
                            loader={<div>Loading Chart</div>}
                            data = {this.state.buy}
                            options={{
                              backgroundColor: 'transparent',
                              title: 'Currency Bought',
                              sliceVisibilityThreshold: 0
                            }}
                            rootProps={{ 'data-testid': '7' }}
                          />
                        </div>
                      </div>
        return content;
    }


}