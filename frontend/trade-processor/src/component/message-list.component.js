import React from 'react';
import {  Subject, takeUntil, map } from 'rxjs';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Paper from '@mui/material/Paper';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import MessageService from '../service/message.service.ts';

export default class MessageListComponent extends React.Component {
    destroy=new Subject();

    constructor(props) {
        super(props);
        this.state = {
            messages:[]
        }
        MessageService.instance().stateChanged.pipe(
            takeUntil(this.destroy),
            map(state=>state.messages)
        ).subscribe(messages=>{
            this.setState({
                messages
            },()=>{
                this.scrollToBottom();
            })
        });
    }

    componentWillUnmount(){
        this.destroy.next();
        this.destroy.complete();
    }

    itemText(msg){
        return `Sell ${msg.currencyFrom}$${msg.amountSell}, Buy  ${msg.currencyTo}$${msg.amountBuy}`;
    }

    scrollToBottom = () => {
        this.listContainer.scrollTop = this.listContainer.scrollHeight;
    }

    render() {
        const hasMessages = this.state.messages.length>0;
        const content = !hasMessages? 
                <div style={{padding:'20px'}}>No Record Found</div> :
                <List>
                    {
                    this.state.messages.map((msg, index) => (
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar>
                            <CurrencyExchangeIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={this.itemText(msg)} secondary={msg.timePlaced} />
                    </ListItem>
                    ))
                    }
                </List>
        return  <Paper ref={(element) => { this.listContainer = element; }}
            style={ {width: '100%', maxWidth: 360,  minWidth: 360, bgcolor: 'background.paper', color: 'black',
            minHeight:600, maxHeight: 600, overflow: 'auto'}}>
            {content}
        </Paper>
    }
}