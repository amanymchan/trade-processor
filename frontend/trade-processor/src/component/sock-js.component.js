import React from 'react';
import SockJsClient from 'react-stomp';
import MessageService from '../service/message.service.ts';
import UserService from '../service/user.service.ts';

export default class SockJsComponent extends React.Component {
    constructor(props) {
      super(props);
      UserService.instance().authenticatUser("amanchan");
    }
  
    onMessage(msg){
      MessageService.instance().addMessage(msg);
    }
  
    render() {
      const customHeaders = {
        "ws-username": UserService.instance().getUserName()
      };  
      return (
        <div>
          <SockJsClient url='http://localhost:8080/websocket' 
              headers = { customHeaders }
              subscribeHeaders = { customHeaders }
              topics={['/user/queue/message']}
              onMessage={(msg) => { this.onMessage(msg) }}
              ref={ (client) => { this.clientRef = client }} 
              debug/>
        </div>
      );
    }
  }